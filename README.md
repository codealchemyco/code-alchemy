# CodeAlchemy

[![pipeline status](https://gitlab.com/codealchemyco/code-alchemy/badges/master/pipeline.svg)](https://gitlab.com/codealchemyco/code-alchemy/commits/master)
[![coverage report](https://gitlab.com/codealchemyco/code-alchemy/badges/master/coverage.svg)](https://gitlab.com/codealchemyco/code-alchemy/commits/master)

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
