FROM bitwalker/alpine-elixir:1.9.0

MAINTAINER drEnilight <romanuk_v@live.ru>

RUN apk update \
    && apk upgrade --no-cache \
    && apk add --no-cache \
      nodejs-npm \
      alpine-sdk \
      openssl-dev \
      inotify-tools \
      imagemagick \
    && mix local.rebar --force \
    && mix local.hex --force

WORKDIR /code_alchemy

COPY . /code_alchemy

RUN mix do deps.get, deps.compile, compile

RUN cd assets && npm install && cd ..
