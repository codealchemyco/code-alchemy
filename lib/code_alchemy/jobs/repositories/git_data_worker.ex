defmodule CodeAlchemy.Repositories.GitDataWorker do
  use GenServer, restart: :transient

  alias CodeAlchemy.Repositories

  # API

  def start_link(repository),
    do: GenServer.start_link(__MODULE__, repository, name: process_name(repository.name))

  # Callbacks

  def init(repository),
    do: {:ok, repository, {:continue, :git_data}}

  def handle_continue(:git_data, repository) do
    Repositories.GitData.perform(repository)
    {:stop, :normal, repository}
  end

  defp process_name(name),
    do: {:via, Registry, {CodeAlchemy.RepositoriesRegistry, "repository_registry_for_#{name}"}}
end
