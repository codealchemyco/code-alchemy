defmodule CodeAlchemy.Repository do
  use Ecto.Schema
  import Ecto.Changeset

  schema "repositories" do
    field :name, :string
    field :body, :string
    field :git_url, :string
    field :languages, :map

    belongs_to :user, CodeAlchemy.User, foreign_key: :user_id
    has_many :branches, CodeAlchemy.Branch

    timestamps([type: :utc_datetime])
  end

  def changeset(repository, params \\ %{}) do
    repository
    |> cast(params, [:name, :body, :git_url, :languages, :user_id])
    |> validate_required([:name, :body, :git_url, :user_id])
    |> unique_constraint(:name, name: :repositories_name_user_id_index)
  end
end
