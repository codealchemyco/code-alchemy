defmodule CodeAlchemy.User do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :password_hash, :string
    field :avatar, CodeAlchemy.Avatar.Type
    field :avatar_url, :string

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    belongs_to :user_role, CodeAlchemy.UserRole, foreign_key: :user_role_id
    has_many :auth_tokens, CodeAlchemy.AuthToken, on_delete: :delete_all

    timestamps([type: :utc_datetime])
  end

  def manual_changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:first_name, :last_name, :email, :password, :password_confirmation])
    |> validate_required([:first_name, :last_name, :email, :password, :password_confirmation])
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/)
    |> validate_length(:password, min: 8, max: 16)
    |> validate_confirmation(:password)
    |> unique_constraint(:email)
    |> put_password_hash
  end

  def github_changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:first_name, :last_name, :email, :avatar_url])
    |> validate_required([:first_name, :last_name, :email, :avatar_url])
    |> validate_format(:email, ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]+$/)
    |> unique_constraint(:email)
  end

  def update_changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:first_name, :last_name, :user_role_id])
    |> cast_attachments(attrs, [:avatar])
    |> validate_required([:first_name, :last_name])
  end

  def avatar_url(user, size) do
    user.avatar_url || CodeAlchemy.Avatar.url({user.avatar, user}, size)
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Bcrypt.hash_pwd_salt(password))
      _ ->
        changeset
    end
  end
end
