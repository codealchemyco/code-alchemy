defmodule CodeAlchemy.UserRole do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_roles" do
    field :name, :string

    has_many :users, CodeAlchemy.User

    timestamps([type: :utc_datetime])
  end

  def changeset(user_role, params \\ %{}) do
    user_role
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
