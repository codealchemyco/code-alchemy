defmodule CodeAlchemy.Commit do
  use Ecto.Schema
  import Ecto.Changeset

  schema "commits" do
    field :hash, :string
    field :author_name, :string
    field :author_email, :string
    field :created_at, :utc_datetime
    field :message, :string

    belongs_to :branch, CodeAlchemy.Branch, foreign_key: :branch_id

    timestamps([type: :utc_datetime])
  end

  def changeset(commit, params \\ %{}) do
    commit
    |> cast(params, [:hash, :author_name, :author_email, :created_at, :message, :branch_id])
    |> validate_required([:hash, :author_name, :author_email, :created_at, :message, :branch_id])
  end
end
