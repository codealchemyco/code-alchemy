defmodule CodeAlchemy.AuthToken do
  use Ecto.Schema
  import Ecto.Changeset

  schema "auth_tokens" do
    field :token, :string
    field :token_type, :string
    field :provider, :string

    belongs_to :user, CodeAlchemy.User, foreign_key: :user_id

    timestamps([type: :utc_datetime])
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:token, :token_type, :provider, :user_id])
    |> validate_required([:token, :token_type, :provider, :user_id])
    |> unique_constraint(:token_type, name: :index_auth_tokens_on_provider)
  end
end
