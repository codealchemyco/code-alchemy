defmodule CodeAlchemy.Branch do
  use Ecto.Schema
  import Ecto.Changeset

  schema "branches" do
    field :name, :string

    belongs_to :repository, CodeAlchemy.Repository, foreign_key: :repository_id
    has_many :commits, CodeAlchemy.Commit

    timestamps([type: :utc_datetime])
  end

  def changeset(branch, params \\ %{}) do
    branch
    |> cast(params, [:name, :repository_id])
    |> validate_required([:name, :repository_id])
  end
end
