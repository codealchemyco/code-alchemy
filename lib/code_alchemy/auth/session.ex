defmodule CodeAlchemy.Auth.Session do
  alias CodeAlchemy.{Repo, User}

  @spec authenticate(map) :: {:ok, User.t} | {:error, String.t}
  def authenticate(params) do
    user = Repo.get_by(User, email: params.email)
    case check_password(user, params.password) do
      true -> {:ok, user}
      _ -> {:error, "Incorrect login credentials"}
    end
  end

  defp check_password(user, password) do
    case user do
      nil -> false
      _ -> Bcrypt.verify_pass(password, user.password_hash)
    end
  end
end
