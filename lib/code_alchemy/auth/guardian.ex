defmodule CodeAlchemy.Auth.Guardian do
  use Guardian, otp_app: :code_alchemy

  import Ecto.Query, only: [from: 2]

  alias CodeAlchemy.{AuthToken, Repo, User}

  def subject_for_token(%User{} = user, _claims), do: {:ok, "User:#{user.id}"}
  def subject_for_token(_), do: {:error, :unhandled_resource_type}

  def resource_from_claims(%{"sub" => "User:" <> user_id}), do: {:ok, Repo.get(User, user_id)}
  def resource_from_claims(_), do: {:error, :unhandled_resource_type}

  def after_encode_and_sign(user, %{"provider" => "manual"} = claims, token, _options) do
    auth_token = AuthToken.changeset(%AuthToken{}, %{token: token,
                                                     token_type: claims["typ"],
                                                     provider: claims["provider"],
                                                     user_id: user.id})

    case Repo.insert(auth_token) do
      {:ok, _} -> {:ok, token}
      {:error, _} -> {:error, "You already logged in"}
    end
  end

  def after_encode_and_sign(user, claims, token, _options) do
    auth_token_changesets = [
      AuthToken.changeset(%AuthToken{}, %{token: token, token_type: claims["typ"], provider: "manual", user_id: user.id}),
      AuthToken.changeset(%AuthToken{}, %{token: claims["token"], token_type: claims["typ"], provider: claims["provider"], user_id: user.id})
    ]

    auth_token_changesets
    |> Enum.with_index()
    |> Enum.reduce(Ecto.Multi.new(), fn ({changeset, index}, multi) ->
       Ecto.Multi.insert(multi, Integer.to_string(index), changeset)
    end)
    |> Repo.transaction

    {:ok, token}
  end

  def on_verify(%{"sub" => "User:" <> user_id, "typ" => token_type} = claims, token, options) do
    query = from at in AuthToken,
              where: at.token == ^token and
                     at.user_id == ^user_id and
                     at.token_type == ^token_type

    if Repo.exists?(query), do: {:ok, claims}, else: {:error, :token_not_found}
  end

  def on_revoke(%{"sub" => "User:" <> user_id, "typ" => token_type} = claims, token, _options) do
    with %AuthToken{} = auth_token <- Repo.get_by(AuthToken, token: token, user_id: user_id, token_type: claims["typ"]),
         {:ok, _auth_token} <- Repo.delete(auth_token) do
      {:ok, claims}
    end
  end
end
