defmodule CodeAlchemy.Auth.Pipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :code_alchemy,
    error_handler: CodeAlchemy.Auth.ErrorHandler,
    module: CodeAlchemy.Auth.Guardian

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.LoadResource, allow_blank: true
  plug CodeAlchemyWeb.Context
end
