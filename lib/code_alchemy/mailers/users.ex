defmodule CodeAlchemy.Mailers.Users do
  use Bamboo.Phoenix, view: CodeAlchemyWeb.EmailView

  def forgot_password_email(user_email, token) do
    new_email
      |> from("no-reply@codealchemy.co")
      |> to(user_email)
      |> put_html_layout({CodeAlchemyWeb.LayoutView, "email.html"})
      |> subject("Forgot password")
      |> assign(:token, token)
      |> render("forgot_password.html")
  end

  def reset_password_email(user_email) do
    new_email
    |> from("no-reply@codealchemy.co")
    |> to(user_email)
    |> put_html_layout({CodeAlchemyWeb.LayoutView, "email.html"})
    |> subject("Reset password")
    |> render("reset_password.html")
  end
end
