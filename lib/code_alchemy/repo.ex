defmodule CodeAlchemy.Repo do
  use Ecto.Repo,
    otp_app: :code_alchemy,
    adapter: Ecto.Adapters.Postgres
end
