defmodule CodeAlchemy.Repositories.GitData do
  alias Ecto.Multi
  alias CodeAlchemy.{Branch, Commit, Repo, Repository}

  def perform(repository) do
    if ready_for_cloning?(repository) do
      repository
      |> clone_project
      |> detect_languages(repository)
      |> list_of_branches
      |> save_branches_and_commits(repository)
      |> delete_local_repo
    end
  end

  defp ready_for_cloning?(repository) do
    {folder_name, path, repository_path} = folder_structure(repository)
    if File.exists?(repository_path), do: delete_local_repo(%GitStreet.Repo{path: repository_path})

    match?({:ok, _}, GitStreet.ls_remote(repository.git_url))
  end

  defp clone_project(repository) do
    {folder_name, path, repository_path} = folder_structure(repository)

    GitStreet.clone(repository.git_url, path: path, folder_name: folder_name)
  end

  defp detect_languages(path, repository) do
    repository
    |> Repository.changeset(%{languages: Babylon.languages(path.path)})
    |> Repo.update

    path
  end

  defp commits_changeset(list_of_commits, branch) do
    Enum.map(list_of_commits, fn commit -> Map.put(commit, :branch_id, branch.id) end)
  end

  defp delete_local_repo(path) do
    File.rm_rf(path.path)
  end

  defp list_of_branches(path) do
    {path, GitStreet.branch(path)}
  end

  defp folder_structure(repository) do
    folder_name = repository.name |> String.replace(~r/\s+/, "") |> Macro.underscore
    path = "#{File.cwd!}/clones"

    {folder_name, path, "#{path}/#{folder_name}"}
  end

  defp save_branches_and_commits({path, list_of_branches}, repository) do
    Enum.each(list_of_branches, fn branch ->
      GitStreet.checkout(path, branch)

      Multi.new()
      |> Multi.insert(:branch, Branch.changeset(%Branch{}, %{name: branch, repository_id: repository.id}))
      |> Multi.merge(fn %{branch: branch} ->
        Multi.new()
        |> Multi.insert_all(:commits, Commit, commits_changeset(GitStreet.log(path), branch))
      end)
      |> Repo.transaction()
    end)

    path
  end
end
