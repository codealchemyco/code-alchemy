defmodule CodeAlchemy.Avatar do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :thumb]

  def validate({file, _}) do
    ~w(.jpg .jpeg .png) |> Enum.member?(Path.extname(file.file_name))
  end

  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 100x100^ -gravity center -extent 100x100 -format png", :png}
  end

  def filename(version, _) do
    version
  end

  def storage_dir(_, {_, scope}) do
    "uploads/user/avatars/#{scope.id}"
  end

  def default_url(_version, _) do
    "assets/images/avatars/default_avatar.jpg"
  end
end
