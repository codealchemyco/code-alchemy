defmodule CodeAlchemyWeb.Schema.Branch do
  use Absinthe.Schema.Notation

  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  import_types(CodeAlchemyWeb.Schema.Commit)

  @desc "Branch object"
  object :branch do
    field :id, :id
    field :name, :string
    field :commits, list_of(:commit), resolve: dataloader(CodeAlchemy.Commit)
  end
end
