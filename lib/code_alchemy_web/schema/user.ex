defmodule CodeAlchemyWeb.Schema.User do
  use Absinthe.Schema.Notation

  import_types Absinthe.Plug.Types
  import CodeAlchemyWeb.Schema.Utils, only: [handle_errors: 1]

  alias CodeAlchemyWeb.Resolvers

  @desc "User object"
  object :user do
    field :id, :id
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :thumb_avatar_url, :string, resolve: fn user, _, _ -> {:ok, CodeAlchemy.User.avatar_url(user, :thumb)} end
  end

  input_object :create_user_params do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :password, :string
    field :password_confirmation, :string
  end

  input_object :update_user_params do
    field :first_name, :string
    field :last_name, :string
    field :avatar, :upload
  end

  @desc "User queries"
  object :user_queries do

    @desc "Get user by id"
    field :user, type: :user do
      arg :id, non_null(:id)
      resolve &Resolvers.User.find/2
    end
  end

  @desc "User mutations"
  object :user_mutations do

    @desc "User registration"
    field :create_user, type: :user do
      arg :user, :create_user_params

      resolve handle_errors(&Resolvers.User.create/2)
    end

    @desc "Update user information"
    field :update_user, type: :user do
      arg :user, :update_user_params

      resolve handle_errors(&Resolvers.User.update/2)
    end

    @desc "Forgot password"
    field :forgot_password, type: :success do
      arg :email, non_null(:string)

      resolve &Resolvers.User.forgot_password/2
    end

    @desc "Reset password"
    field :reset_password, type: :success do
      arg :token, non_null(:string)
      arg :password, non_null(:string)
      arg :password_confirmation, non_null(:string)

      resolve &Resolvers.User.reset_password/2
    end
  end
end
