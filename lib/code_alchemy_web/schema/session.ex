defmodule CodeAlchemyWeb.Schema.Session do
  use Absinthe.Schema.Notation
  alias CodeAlchemyWeb.Resolvers

  @desc "Session object"
  object :session do
    field :token, :string
  end

  @desc "Session mutations"
  object :session_mutations do
    @desc "User login"
    field :sign_in, type: :session do
      arg :email, non_null(:string)
      arg :password, non_null(:string)

      resolve &Resolvers.Session.sign_in/2
    end

    @desc "User logout"
    field :sign_out, type: :user do
      arg :token, non_null(:string)

      resolve &Resolvers.Session.sign_out/2
    end
  end
end
