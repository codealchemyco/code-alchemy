defmodule CodeAlchemyWeb.Schema.Commit do
  use Absinthe.Schema.Notation

  @desc "Commit object"
  object :commit do
    field :id, :id
    field :hash, :string
    field :author_name, :string
    field :author_email, :string
    field :created_at, :datetime
    field :message, :string
  end
end
