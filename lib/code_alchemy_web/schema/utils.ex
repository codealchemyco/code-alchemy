defmodule CodeAlchemyWeb.Schema.Utils do
  @moduledoc """
  Helper functions for CodeAlchemyWeb.Schema
  """

  def handle_errors(fun) do
    fn source, args, info ->
      case Absinthe.Resolution.call(fun, source, args, info) do
        {:error, %Ecto.Changeset{} = changeset} -> format_changeset(changeset)
        val -> val
      end
    end
  end

  def format_changeset(changeset) do
    errors =
      changeset.errors
      |> Enum.map(fn {key, {value, _context}} ->
        field = key |> to_string |> String.capitalize
        [message: "#{field} #{value}", details: key]
      end)

    {:error, errors}
  end
end
