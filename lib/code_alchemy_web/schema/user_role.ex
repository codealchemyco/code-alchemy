defmodule CodeAlchemyWeb.Schema.UserRole do
  use Absinthe.Schema.Notation

  alias CodeAlchemyWeb.Resolvers

  @desc "UserRole object"
  object :user_role do
    field :id, :id
    field :name, :string
  end

  @desc "UserRole queries"
  object :user_role_queries do

    @desc "Get all roles"
    field :user_roles, list_of(:user_role) do
      resolve &Resolvers.UserRole.all/2
    end
  end
end
