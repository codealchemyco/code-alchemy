defmodule CodeAlchemyWeb.Schema.Repository do
  use Absinthe.Schema.Notation

  import Absinthe.Resolution.Helpers, only: [dataloader: 1]
  import CodeAlchemyWeb.Schema.Utils, only: [handle_errors: 1]

  alias CodeAlchemyWeb.Resolvers

  import_types(CodeAlchemyWeb.Schema.Branch)

  @desc "Repository object"
  object :repository do
    field :id, :id
    field :name, :string
    field :body, :string
    field :git_url, :string
    field :user_id, :integer
    field :languages, list_of(:language) do
      resolve(fn repository, _, _ ->
        languages = Enum.map(repository.languages, fn {name, percent} -> %{name: name, percent: percent} end)
        {:ok, languages}
      end)
    end
    field :branches, list_of(:branch), resolve: dataloader(CodeAlchemy.Branch)
  end

  @desc "Language object"
  object :language do
    field :name, :string
    field :percent, :float
  end

  @desc "Repository queries"
  object :repository_queries do

    @desc "Get all repositories"
    field :repositories, list_of(:repository) do
      resolve &Resolvers.Repository.all/2
    end
  end

  input_object :create_repository_params do
    field :name, non_null(:string)
    field :body, non_null(:string)
    field :git_url, non_null(:string)
  end

  input_object :update_repository_params do
    field :name, :string
    field :body, :string
    field :git_url, :string
  end

  @desc "Repository mutations"
  object :repository_mutations do

    @desc "Create repository"
    field :create_repository, type: :repository do
      arg :repository, :create_repository_params

      resolve handle_errors(&Resolvers.Repository.create/2)
    end

    @desc "Update repository data"
    field :update_repository, type: :repository do
      arg :id, non_null(:integer)
      arg :repository, :update_repository_params

      resolve handle_errors(&Resolvers.Repository.update/2)
    end

    @desc "Delete repository"
    field :delete_repository, type: :success do
      arg :id, non_null(:integer)

      resolve &Resolvers.Repository.delete/2
    end
  end
end
