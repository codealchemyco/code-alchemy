defmodule CodeAlchemyWeb.Schema do
  use Absinthe.Schema

  alias CodeAlchemy.{Branch, Commit, Repo}

  @desc "Success object"
  object :success do
    field :message, :string
  end

  import_types Absinthe.Type.Custom
  import_types CodeAlchemyWeb.Schema.User
  import_types CodeAlchemyWeb.Schema.Session
  import_types CodeAlchemyWeb.Schema.UserRole
  import_types CodeAlchemyWeb.Schema.Repository

  def context(data) do
    default_query = fn queryable, _params -> queryable end
    default_source = Dataloader.Ecto.new(Repo, query: default_query)

    loader =
      Dataloader.new()
      |> Dataloader.add_source(Branch, default_source)
      |> Dataloader.add_source(Commit, default_source)

    Map.put(data, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end

  query do
    import_fields :user_queries
    import_fields :user_role_queries
    import_fields :repository_queries
  end

  mutation do
    import_fields :user_mutations
    import_fields :session_mutations
    import_fields :repository_mutations
  end
end
