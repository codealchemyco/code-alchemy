defmodule CodeAlchemyWeb.PageController do
  use CodeAlchemyWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
