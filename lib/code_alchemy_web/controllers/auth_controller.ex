defmodule CodeAlchemyWeb.AuthController do
  use CodeAlchemyWeb, :controller

  alias Ueberauth.Strategy.Helpers
  alias CodeAlchemy.{Auth, Repo, User}

  plug Ueberauth

  def request(conn, _params) do
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    redirect(conn, to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    [first_name|[last_name | _]] = String.split(auth.info.name, " ")

    user_params = %{
      email: auth.info.email,
      first_name: first_name,
      last_name: last_name,
      avatar_url: auth.info.image
    }

    sign_in(conn, %User{} |> User.github_changeset(user_params))
  end

  defp sign_in(%{assigns: %{ueberauth_auth: auth}} = conn, changeset) do
    {:ok, user} = insert_or_update_user(changeset)
    {:ok, token, _} = Auth.Guardian.encode_and_sign(user, %{provider: conn.params["provider"], token: auth.credentials.token}, token_type: :access)

    conn
    |> json(%{token: token, provider_token: auth.credentials.token})
  end

  defp insert_or_update_user(changeset) do
    case Repo.get_by(User, email: changeset.changes.email) do
      nil -> Repo.insert(changeset)
      user -> {:ok, user}
    end
  end
end
