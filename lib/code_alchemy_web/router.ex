defmodule CodeAlchemyWeb.Router do
  use CodeAlchemyWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :graphql do
    plug CodeAlchemy.Auth.Pipeline
  end

  if Mix.env == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  scope "/", CodeAlchemyWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api" do
    pipe_through :graphql

    forward "/", Absinthe.Plug, schema: CodeAlchemyWeb.Schema
  end

  forward "/graphiql", Absinthe.Plug.GraphiQL, schema: CodeAlchemyWeb.Schema

  scope "/auth", CodeAlchemyWeb do
    pipe_through :browser

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
  end
end
