defmodule CodeAlchemyWeb.Resolvers.Session do
  alias CodeAlchemy.Auth

  @spec sign_in(map, map) :: {:ok, %{token: String.t}}
  def sign_in(params, _info) do
    with {:ok, user} <- Auth.Session.authenticate(params),
         {:ok, token, _} <- Auth.Guardian.encode_and_sign(user, %{provider: "manual"}, token_type: :access) do
      {:ok, %{token: token}}
    end
  end

  @spec sign_out(map, map) :: {:ok, User.t} | {:error, String.t}
  def sign_out(%{token: token}, %{context: %{current_user: %{id: user_id}}}) do
    case Auth.Guardian.resource_from_token(token) do
      {:ok, %{id: ^user_id}, _claims} -> Auth.Guardian.revoke(token)
      _ -> {:error, "Token not found"}
    end
  end

  def sign_out(_params, _info) do
    {:error, "You need to be logged-in"}
  end
end
