defmodule CodeAlchemyWeb.Resolvers.User do
  alias CodeAlchemy.{AuthToken, Mailer, Mailers, Repo, User}

  import Ecto.Query, only: [from: 2]

  @spec find(map, map) :: {:ok, User.t} | {:error, String.t}
  def find(%{id: id}, %{context: %{current_user: _user}}) do
    case Repo.get(User, id) do
      nil  -> {:error, "User #{id} not found"}
      user -> {:ok, user}
    end
  end

  def find(_params, _info) do
    {:error, "You need to be logged-in"}
  end

  @spec create(map, map) :: {:ok, User.t}
  def create(%{user: user_params}, _info) do
    %User{}
    |> User.manual_changeset(user_params)
    |> Repo.insert
  end

  @spec update(map, map) :: {:ok, User.t}
  def update(%{user: user_params}, %{context: %{current_user: user}}) do
    user
    |> User.update_changeset(user_params)
    |> Repo.update
  end

  def update(_params, _info) do
    {:error, "You need to be logged-in"}
  end

  @spec forgot_password(map, map) :: {:ok, User.t}
  def forgot_password(%{email: email}, _info) do
    case Repo.get_by(User, email: email) do
      nil -> {:error, "User email #{email} not found!"}
      user -> generate_token(user)
    end
  end

  @spec reset_password(map, map) :: {:ok, User.t}
  def reset_password(%{token: token, password: password, password_confirmation: password_confirmation}, _info) do
    case Repo.get_by(AuthToken, token: token, token_type: "reset_password") do
      nil -> {:error, "You cannot reset your password"}
      auth_token -> update_user_password(auth_token, password, password_confirmation)
    end
  end

  defp generate_token(user) do
    case Repo.exists?(from at in AuthToken, where: at.user_id == ^user.id and at.token_type == "reset_password" and at.provider == "manual") do
      true -> {:error, "Email has already sent"}
      false -> create_reset_password_token(user)
    end
  end

  defp create_reset_password_token(user) do
    token = JOSE.JWS.generate_key(%{"alg" => "HS512"}) |> JOSE.JWK.to_map |> elem(1) |> Map.get("k")

    %AuthToken{}
    |> AuthToken.changeset(%{token: token, token_type: "reset_password", provider: "manual", user_id: user.id})
    |> Repo.insert

    Mailers.Users.forgot_password_email(user.email, token)
    |> Mailer.deliver_later

    {:ok, %{message: "Email was sent successfully"}}
  end

  defp update_user_password(auth_token, password, password_confirmation) do
    query = from u in User,
                    join: at in AuthToken, on: at.user_id == u.id,
                    where: at.token == ^auth_token.token

    user = Repo.one(query)

    user
    |> User.manual_changeset(%{password: password, password_confirmation: password_confirmation})
    |> Repo.update

    Repo.delete(auth_token)

    Mailers.Users.reset_password_email(user.email)
    |> Mailer.deliver_later

    {:ok, %{message: "Your password has been changed"}}
  end
end
