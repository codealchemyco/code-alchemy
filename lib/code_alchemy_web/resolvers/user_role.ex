defmodule CodeAlchemyWeb.Resolvers.UserRole do
  alias CodeAlchemy.{Repo, UserRole}

  @spec all(map, map) :: {:ok, list} | {:error, String.t}
  def all(_params, %{context: %{current_user: _user}}) do
    {:ok, Repo.all(UserRole)}
  end

  def all(_params, _info) do
    {:error, "You need to be logged-in"}
  end
end
