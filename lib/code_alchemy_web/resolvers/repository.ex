defmodule CodeAlchemyWeb.Resolvers.Repository do
  alias CodeAlchemy.{Repo, Repositories, Repository}

  import Ecto.Query, only: [where: 2]

  @spec all(map, map) :: {:ok, list} | {:error, String.t}
  def all(_params, %{context: %{current_user: %{id: id}}}) do
    repositories =
      Repository
      |> where(user_id: ^id)
      |> Repo.all

    {:ok, repositories}
  end

  def all(_params, _info) do
    {:error, "You need to be logged-in"}
  end

  @spec create(map, map) :: {:ok, Repository.t}
  def create(%{repository: repository_params}, %{context: %{current_user: %{id: user_id}}}) do
    result =
      %Repository{user_id: user_id}
      |> Repository.changeset(repository_params)
      |> Repo.insert

    case result do
      {:ok, repository} -> Repositories.GitDataWorker.start_link(repository)
      _ -> nil
    end

    result
  end

  def create(_params, _info) do
    {:error, "You need to be logged-in"}
  end

  @spec update(map, map) :: {:ok, Repository.t}
  def update(%{id: id, repository: repository_params}, %{context: %{current_user: %{id: user_id}}}) do
    case Repo.get_by(Repository, [id: id, user_id: user_id]) do
      nil -> {:error, "Repository was not found"}
      repository -> repository |> Repository.changeset(repository_params) |> Repo.update
    end
  end

  def update(_params, _info) do
    {:error, "You need to be logged-in"}
  end

  @spec delete(map, map) :: {:ok, Repository.t}
  def delete(%{id: id}, %{context: %{current_user: %{id: user_id}}}) do
    case Repo.get_by(Repository, [id: id, user_id: user_id]) do
      nil -> {:error, "Repository was not found"}
      repository -> Repo.delete(repository)
                    {:ok, %{message: "Your repository has been deleted"}}
    end
  end

  def delete(_params, _info) do
    {:error, "You need to be logged-in"}
  end
end
