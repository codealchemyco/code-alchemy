defmodule CodeAlchemy.MixProject do
  use Mix.Project

  def project do
    [
      app: :code_alchemy,
      version: "0.1.0",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      preferred_cli_env: [espec: :test, coveralls: :test],
      test_coverage: [tool: ExCoveralls, test_task: "espec"],
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {CodeAlchemy.Application, []},
      extra_applications: [:logger, :runtime_tools, :ueberauth, :ueberauth_github, :peerage]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "spec/support", "spec/factories"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:phoenix, "~> 1.4.9"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_ecto, "~> 4.0"},
      {:ecto_sql, "~> 3.2.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:guardian, "~> 1.2"},
      {:absinthe, "~> 1.4.0"},
      {:absinthe_plug, "~> 1.4.0"},
      {:dataloader, "~> 1.0.4"},
      {:bcrypt_elixir, "~> 2.0"},
      {:poison, "~> 3.1"},
      {:espec, "~> 1.7.0", only: :test},
      {:ex_machina, "~> 2.3", only: :test},
      {:ueberauth, "~> 0.6"},
      {:ueberauth_github, "~> 0.8.0"},
      {:arc, "~> 0.11.0"},
      {:arc_ecto, "~> 0.11.1"},
      {:peerage, "~> 1.0.2"},
      {:bamboo, "~> 1.3"},
      {:bamboo_smtp, "~> 2.1.0"},
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12.1", only: :test},
      {:git_street, "~> 0.1.4"},
      {:babylon, "~> 0.1.2"}
    ]
  end

  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "phx.start": ["ecto.create --quiet", "ecto.migrate", "phx.server"],
      test: ["ecto.create --quiet", "ecto.migrate", "run priv/repo/seeds.exs", "espec"]
    ]
  end
end
