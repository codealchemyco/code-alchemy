defmodule CodeAlchemy.Repo.Migrations.AddLanguagesToRepository do
  use Ecto.Migration

  def change do
    alter table(:repositories) do
      add :languages, :map
    end
  end
end
