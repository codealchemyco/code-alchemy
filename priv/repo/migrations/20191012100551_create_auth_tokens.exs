defmodule CodeAlchemy.Repo.Migrations.CreateAuthTokens do
  use Ecto.Migration

  def change do
    create table(:auth_tokens) do
      add :token, :text
      add :token_type, :string
      add :provider, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:auth_tokens, [:token_type, :provider, :user_id], name: :index_auth_tokens_on_provider)
  end
end
