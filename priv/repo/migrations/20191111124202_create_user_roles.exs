defmodule CodeAlchemy.Repo.Migrations.CreateUserRoles do
  use Ecto.Migration

  def change do
    create table(:user_roles) do
      add :name, :string

      timestamps()
    end

    alter table(:users) do
      add :user_role_id, references(:user_roles)
    end
  end
end
