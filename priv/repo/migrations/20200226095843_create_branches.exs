defmodule CodeAlchemy.Repo.Migrations.CreateBranches do
  use Ecto.Migration

  def change do
    create table(:branches) do
      add :name, :string
      add :repository_id, references(:repositories, on_delete: :delete_all)

      timestamps()
    end

    create index(:branches, [:repository_id])
  end
end
