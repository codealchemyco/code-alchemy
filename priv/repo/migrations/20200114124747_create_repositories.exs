defmodule CodeAlchemy.Repo.Migrations.CreateRepositories do
  use Ecto.Migration

  def change do
    create table(:repositories) do
      add :name, :string
      add :body, :string
      add :git_url, :string
      add :user_id, references(:users)

      timestamps()
    end

    create index(:repositories, [:user_id])
    create unique_index(:repositories, [:name, :user_id])
  end
end
