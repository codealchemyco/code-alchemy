defmodule CodeAlchemy.Repo.Migrations.CreateCommits do
  use Ecto.Migration

  def change do
    create table(:commits) do
      add :hash, :string
      add :author_name, :string
      add :author_email, :string
      add :created_at, :utc_datetime
      add :message, :string
      add :branch_id, references(:branches, on_delete: :delete_all)

      add :inserted_at, :utc_datetime, default: fragment("timezone('utc', now())"), null: false
      add :updated_at, :utc_datetime, default: fragment("timezone('utc', now())"), null: false
    end

    create index(:commits, [:branch_id])
  end
end
