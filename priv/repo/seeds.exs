# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CodeAlchemy.Repo.insert!(%CodeAlchemy.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias CodeAlchemy.{Repo, UserRole}

Enum.each(["manager", "developer", "admin"], fn(role) ->
  case Repo.get_by(UserRole, name: role) do
    nil  -> Repo.insert! %UserRole{name: role}
    user_role -> {:ok, user_role}
  end
end)
