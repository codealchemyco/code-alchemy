all: prepare-minikube create

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "$(YELLOW)%-20s$(NO_COLOR) %s\n", $$1, $$2}'

# Kubernetes helper functions

prepare-minikube: ## Prepare minikube env
	@eval $(minikube docker-env)

create: ## Create kubernetes cluster
	kubectl -n code-alchemy-namespace create -f k8s/namespace.yaml
	kubectl -n code-alchemy-namespace create -f k8s/pvc.yaml
	kubectl -n code-alchemy-namespace create -f k8s/deployment/db.yaml
	kubectl -n code-alchemy-namespace create -f k8s/deployment/code-alchemy.yaml
	kubectl -n code-alchemy-namespace create -f k8s/services/db.yaml
	kubectl -n code-alchemy-namespace create -f k8s/services/code-alchemy.yaml
	kubectl -n code-alchemy-namespace create -f k8s/ingress.yaml

destroy: ## Destroy kubernetes cluster
	kubectl -n code-alchemy-namespace delete -f k8s/ingress.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/services/code-alchemy.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/services/db.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/deployment/code-alchemy.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/deployment/db.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/pvc.yaml
	kubectl -n code-alchemy-namespace delete -f k8s/namespace.yaml

get_all: ## Get all information about pods and ingress
	kubectl -n code-alchemy-namespace get all,ingress

# Docker helper functions

build_release:
	docker build -f Dockerfile.release -t code_alchemy_release:latest .
