FROM bitwalker/alpine-elixir:1.9.0 AS builder

ENV MIX_ENV=prod

WORKDIR /usr/local/code_alchemy

# This step installs all the build tools we'll need
RUN apk update \
    && apk upgrade --no-cache \
    && apk add --no-cache \
      nodejs-npm \
      alpine-sdk \
      openssl-dev \
    && mix local.rebar --force \
    && mix local.hex --force

# Copies our app source code into the build container
COPY . .

# Compile Elixir
RUN mix do deps.get, deps.compile, compile

# Compile Javascript
RUN cd assets \
    && npm install \
    && ./node_modules/webpack/bin/webpack.js --mode production \
    && cd .. \
    && mix phx.digest

# Build Release
RUN mkdir -p /opt/release \
    && mix release \
    && mv _build/${MIX_ENV}/rel/code_alchemy /opt/release

# Create the runtime container
FROM bitwalker/alpine-erlang:22 as runtime

# Install runtime dependencies
RUN apk update \
    && apk upgrade --no-cache \
    && apk add --no-cache gcc \
    && apk add imagemagick

WORKDIR /usr/local/code_alchemy

COPY --from=builder /opt/release/code_alchemy .

COPY entrypoint.sh .

CMD ["./entrypoint.sh"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=2 \
 CMD nc -vz -w 2 localhost 4000 || exit 1
