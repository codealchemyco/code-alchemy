#!/bin/sh
# Docker entrypoint script.

# Wait until Postgres is ready
while ! nc -z $DATABASE_HOST 5432
do
  echo "$(date) - waiting for database to start"
  sleep 2
done

bin/code_alchemy eval CodeAlchemy.Release.migrate

bin/code_alchemy start
