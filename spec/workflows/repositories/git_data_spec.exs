defmodule CodeAlchemy.Repositories.GitDataSpec do
  import CodeAlchemy.Factory
  use ESpec

  alias CodeAlchemy.{Repo, Branch, Commit}

  let user: insert(:user)
  let repository: insert(:repository, name: "Code Alchemy", user: user, git_url: git_url)

  subject do: fn -> described_module.perform(repository) end

  context "with valid git_url" do
    let git_url: "https://gitlab.com/qnVictoria/codealchemy_test.git"

    it "returns created branches and commits" do
      is_expected |> to(change fn -> Repo.aggregate(Branch, :count, :id) end, by: 3)
      is_expected |> to(change fn -> Repo.aggregate(Commit, :count, :id) end, by: 8)
    end

    context "with existing project folder" do
      subject do: described_module.perform(repository)

      before do: File.mkdir("#{File.cwd!}/clones/code_alchemy")

      it "returns result" do
        is_expected |> to(be_ok_result)
      end
    end
  end

  context "with invalid git_url" do
    let git_url: "invalid git_url"

    it "returns result without changes" do
      is_expected |> to(change fn -> Repo.aggregate(Branch, :count, :id) end, by: 0)
      is_expected |> to(change fn -> Repo.aggregate(Commit, :count, :id) end, by: 0)
    end
  end
end
