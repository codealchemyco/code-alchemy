defmodule CodeAlchemy.Acceptance.Session do
  import CodeAlchemy.Factory
  use ESpec

  let process_query: query |> Absinthe.run(CodeAlchemyWeb.Schema, variables: variables, context: context)

  describe "Resolver: Сreate user session" do
    let! user: insert(:user, email: "jon@west.com", password_hash: Bcrypt.hash_pwd_salt("west1234"))
    let context: %{}
    let query: """
      mutation signIn($email: String, $password: String) {
        signIn(email: $email, password: $password) {
          token
        }
      }
    """

    context "with valid user credentials" do
      let variables: %{"email" => "jon@west.com", "password" => "west1234"}

      it "returns token" do
        {:ok, %{data: %{"signIn" => result}}} = process_query

        expect result |> to(have_key "token")
      end

    context "when user already logged in" do
      let! token: insert(:auth_token, token_type: "access", provider: "manual", user: user)

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You already logged in"})
      end
    end
   end
  end

  describe "Resolver: Close user session" do
    let user: insert(:user)
    let context: %{current_user: user}
    let query: """
    mutation signOut($token: String) {
      signOut(token: $token) {
        id
        first_name
        last_name
        email
      }
    }
    """

    context "when token belongs to user" do
      let :variables do
        {:ok, token, _} = CodeAlchemy.Auth.Guardian.encode_and_sign(user, %{provider: "manual"}, token_type: :access)
        %{"token" => token}
      end

    before do: process_query

      it "returns result" do
        expect CodeAlchemy.Auth.Guardian.decode_and_verify(variables["token"]) |> to (eq {:error, :token_not_found})
      end
    end

    context "when token belongs to another_user" do
      let another_user: insert(:user)
      let :variables do
        {:ok, token, _} = CodeAlchemy.Auth.Guardian.encode_and_sign(another_user, %{provider: "manual"}, token_type: :access)
        %{"token" => token}
      end

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "Token not found"})
      end
    end
  end
end
