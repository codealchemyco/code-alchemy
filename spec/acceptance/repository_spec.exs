defmodule CodeAlchemy.Acceptance.Repository do
  import CodeAlchemy.Factory
  use ESpec

  let user: insert(:user)

  let process_query: query |> Absinthe.run(CodeAlchemyWeb.Schema, variables: variables, context: context)

  describe "Resolver: Get all repositories" do
    let variables: %{}
    let query: """
      query getAllRepositories {
        repositories {
          id
          name
          body
          gitUrl
          languages {
            name
            percent
          }
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}
      let! my_repo: insert(:repository, user: user, languages: %{"Ruby" => 23.5, "Elixir" => 76.5})
      let! another_repo: insert(:repository)

      it "returns list of repositories" do
        {:ok, %{data: %{"repositories" => [repository| _] = result}}} = process_query

        expect result |> to(have_size 1)
        expect repository |> to(have {"id", "#{my_repo.id}"})
        expect repository |> to(have {"languages", [%{"name" => "Elixir", "percent" => 76.5},
                                                    %{"name" => "Ruby", "percent" => 23.5}]})
      end

      context "with branches and commits" do
        let! branch: insert(:branch, repository: my_repo)
        let! commit: insert(:commit, branch: branch)
        let query: """
          query getBranchesAndCommits {
            repositories {
              id
              branches {
                id
                commits {
                  id
                }
              }
            }
          }
        """

        it "returns list of repositories" do
          {:ok, %{data: %{"repositories" => result}}} = process_query

          expect result |> to(eq [%{"branches" => [%{"commits" => [%{"id" => "#{commit.id}"}], "id" => "#{branch.id}"}], "id" => "#{my_repo.id}"}])
        end
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end
  end

  describe "Resolver: Create repository" do
    let variables: %{"repository" => %{"name" => "project_1",
                                       "body" => "body_1",
                                       "gitUrl" => "git@github.com:project_1.git"}}
    let query: """
      mutation createRepository($repository: CreateRepositoryParams!) {
        createRepository(repository: $repository) {
          name
          body
          gitUrl
          userId
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      context "with valid repository params" do
        it "returns created repository" do
          expect process_query |> to(eq {:ok, %{data: %{"createRepository" => %{"name" => "project_1",
                                                                                "body" => "body_1",
                                                                                "gitUrl" => "git@github.com:project_1.git",
                                                                                "userId" => user.id}}}})
        end
      end

      context "with invalid repository params" do
        let! my_repo: insert(:repository, user: user, name: "project_2")
        let variables: %{"repository" => %{"name" => "project_2",
                                           "body" => "body_2",
                                           "gitUrl" => "git@github.com:project_2.git"}}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "Name has already been taken"})
        end
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end
  end

  describe "Resolver: Update repository" do
    let user: insert(:user)
    let! my_repo: insert(:repository, user: user, name: "project_1")
    let variables: %{"id" => my_repo.id, "repository" => %{"name" => "project_1.1", "body" => "body_1.1"}}
    let query: """
      mutation updateRepository($id: ID!, $repository: UpdateRepositoryParams!) {
        updateRepository(id: $id, repository: $repository) {
          name
          body
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      context "with valid repository params" do
        it "returns updated repository" do
          expect process_query |> to(eq {:ok, %{data: %{"updateRepository" => %{"name" => "project_1.1",
                                                                                "body" => "body_1.1"}}}})
        end
      end

      context "when repository doesn`t belong to user" do
        let! another_repo: insert(:repository)
        let variables: %{"id" => another_repo.id, "repository" => %{"name" => "project_1.1", "body" => "body_1.1"}}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "Repository was not found"})
        end
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end
  end

  describe "Resolver: Delete repository" do
    let user: insert(:user)
    let! my_repo: insert(:repository, user: user)
    let query: """
      mutation deleteRepository($id: ID!) {
        deleteRepository(id: $id) {
          message
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      context "when repository belongs to the user" do
        let variables: %{"id" => my_repo.id}

        it "returns success message" do
          expect process_query |> to(eq {:ok, %{data: %{"deleteRepository" => %{"message" => "Your repository has been deleted"}}}})
        end
      end

      context "when repository doesn`t belong to user" do
        let! another_repo: insert(:repository)
        let variables: %{"id" => another_repo.id}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "Repository was not found"})
        end
      end

      context "when user is not authorized" do
        let context: %{}
        let variables: %{"id" => my_repo.id}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "You need to be logged-in"})
        end
      end
    end
  end
end
