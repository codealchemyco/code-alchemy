defmodule CodeAlchemy.Acceptance.User do
  import CodeAlchemy.Factory
  use ESpec

  alias CodeAlchemy.{Auth, AuthToken}

  import Ecto.Query, only: [from: 2]

  import Phoenix.ConnTest, except: [conn: 0, build_conn: 0]
  @endpoint CodeAlchemyWeb.Endpoint

  let process_query: query |> Absinthe.run(CodeAlchemyWeb.Schema, variables: variables, context: context)

  describe "Resolver: Get user by id" do
    let user: insert(:user, first_name: "Lady", last_name: "Gaga", email: "lady@gaga.com")
    let variables: %{"id" => user.id}
    let query: """
      query getUserById($id: ID!) {
        user(id: $id) {
          firstName
          lastName
          email
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      it "returns expected user" do
        expect process_query |> to(eq {:ok, %{data: %{"user" => %{"firstName" => "Lady",
                                                                  "lastName" => "Gaga",
                                                                  "email" => "lady@gaga.com"}}}})
      end

      context "when finding non-existing user" do
        let variables: %{"id" => 0}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "User 0 not found"})
        end
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end
  end

  describe "Resolver: Create user" do
    let context: %{}
    let query: """
      mutation createUser($user: CreateUserParams!) {
        createUser(user: $user) {
          firstName
          lastName
          email
        }
      }
    """

    context "with valid user params" do
      let variables: %{"user" => %{"firstName" => "Jack",
                                   "lastName" => "Rassel",
                                   "email" => "jack@ras.com",
                                   "password" => "rty12345",
                                   "password_confirmation" => "rty12345"}}

      it "returns created user" do
        expect process_query |> to(eq {:ok, %{data: %{"createUser" => %{"firstName" => "Jack",
                                                                        "lastName" => "Rassel",
                                                                        "email" => "jack@ras.com"}}}})
      end
    end

    context "with invalid user password_confirmation" do
      let variables: %{"user" => %{"firstName" => "Jack",
                                   "lastName" => "Rassel",
                                   "email" => "jack@ras.com",
                                   "password" => "rty12345",
                                   "password_confirmation" => "qwe12345"}}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "Password_confirmation does not match confirmation"})
      end
    end
  end

  describe "Resolver: Update user" do
    let user: insert(:user)
    let variables: %{"user" => %{"firstName" => "Jack",
                                 "lastName" => "Rassel"}}
    let query: """
      mutation updateUser($user: UpdateUserParams!) {
        updateUser(user: $user) {
          firstName
          lastName
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      it "returns updated user" do
        expect process_query |> to(eq {:ok, %{data: %{"updateUser" => %{"firstName" => "Jack",
                                                                        "lastName" => "Rassel"}}}})
      end

      context "with invalid user params" do
        let variables: %{"user" => %{"firstName" => "",
                                     "lastName" => "Rassel"}}

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "First_name can't be blank"})
        end
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end

    context "with avatar uploading" do
      let avatar: %Plug.Upload{content_type: "multipart/form-data",
                               filename: "elixir-logo.jpg",
                               path: Path.expand("../fixtures/images/elixir-logo.jpg", __DIR__)}
      let query: """
        mutation {
          updateUser(user: { avatar: "avatar_attribute_arbitraty_name" }){
            thumbAvatarUrl
          }
        }
      """
      let conn: Phoenix.ConnTest.build_conn()

      it "returns updated user" do
        {:ok, token, _} = Auth.Guardian.encode_and_sign(user, %{provider: "manual"}, token_type: :access)
        resp = conn
               |> Plug.Conn.put_req_header("authorization", "Bearer #{token}")
               |> Plug.Conn.put_req_header("content-type", "application/json")
               |> post("/api", %{"query" => query, "avatar_attribute_arbitraty_name" => avatar})

        expect resp.status |> to eq(200)
      end
    end
  end

  describe "Resolver: Forgot password" do
    let context: %{}
    let query: """
      mutation forgotPassword($email: String) {
        forgotPassword(email: $email) {
          message
        }
      }
    """

    context "when user exists" do
      let user: insert(:user)
      let variables: %{"email" => user.email}

      context "reset_password token exists" do
        let! token: insert(:auth_token, token_type: "reset_password", provider: "manual", user: user)

        it "returns error" do
          {:ok, %{errors: [error| _t]}} = process_query

          expect error |> to(have {:message, "Email has already sent"})
        end
      end

      context "reset_password token doesn`t exist" do
        let auth_token_query: from at in AuthToken, where: at.user_id == ^user.id and at.token_type == "reset_password" and at.provider == "manual"

        it "returns success message" do
          expect process_query |> to(eq {:ok, %{data: %{"forgotPassword" => %{"message" => "Email was sent successfully"}}}})
        end
      end
    end

    context "when user doesn`t exist" do
      let variables: %{"email" => "jack@ras.com"}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "User email jack@ras.com not found!"})
      end
    end
  end

  describe "Resolver: Reset password" do
    let context: %{}
    let user: insert(:user)
    let variables: %{"token" => token, "password" => "zxcv12345", "password_confirmation" => "zxcv12345"}
    let query: """
      mutation resetPassword($token: String, $password: String, $password_confirmation: String) {
        resetPassword(token: $token, password: $password, password_confirmation: $password_confirmation) {
          message
        }
      }
    """

    context "when reset_password token exists" do
      let reset_password_token: insert(:auth_token, token_type: "reset_password", user: user)
      let token: reset_password_token.token

      it "returns success message" do
        expect process_query |> to(eq {:ok, %{data: %{"resetPassword" => %{"message" => "Your password has been changed"}}}})
      end
    end

    context "when reset_password token doesn`t exist" do
      let token: "random_token"

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You cannot reset your password"})
      end
    end
  end
end
