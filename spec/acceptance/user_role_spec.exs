defmodule CodeAlchemy.Acceptance.UserRole do
  import CodeAlchemy.Factory
  use ESpec

  let process_query: query |> Absinthe.run(CodeAlchemyWeb.Schema, variables: variables, context: context)

  describe "Resolver: Get all roles" do
    let user: insert(:user)
    let variables: %{}
    let query: """
      query getAllUserRoles {
        user_roles {
          name
        }
      }
    """

    context "when user is already authorized" do
      let context: %{current_user: user}

      it "returns expected user" do
        {:ok, %{data: %{"user_roles" => user_roles}}} = process_query
        role_names = Enum.map(user_roles, fn user_role -> user_role["name"] end)

        expect role_names |> to(contain_exactly ["manager", "developer", "admin"])
      end
    end

    context "when user is not authorized" do
      let context: %{}

      it "returns error" do
        {:ok, %{errors: [error| _t]}} = process_query

        expect error |> to(have {:message, "You need to be logged-in"})
      end
    end
  end
end
