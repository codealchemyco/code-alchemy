defmodule CodeAlchemy.Factory do
  use ExMachina.Ecto, repo: CodeAlchemy.Repo
  use CodeAlchemy.{AuthTokenFactory, RepositoryFactory, UserFactory, BranchFactory, CommitFactory}
end
