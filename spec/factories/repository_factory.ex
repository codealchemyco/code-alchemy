defmodule CodeAlchemy.RepositoryFactory do
  defmacro __using__(_opts) do
    quote do
      def repository_factory do
        %CodeAlchemy.Repository{
          name: sequence(:name, &"name#{&1}"),
          body: sequence(:body, &"body#{&1}"),
          git_url: sequence(:git_url, &"git@github.com:project_#{&1}.git"),
          user: build(:user)
        }
      end
    end
  end
end
