defmodule CodeAlchemy.AuthTokenFactory do
  defmacro __using__(_opts) do
    quote do
      def auth_token_factory do
        %CodeAlchemy.AuthToken{
          token: Ecto.UUID.generate |> binary_part(0, 20),
          token_type: sequence(:token_type, ["access", "reset_password"]),
          provider: "manual",
          user: build(:user)
        }
      end
    end
  end
end
