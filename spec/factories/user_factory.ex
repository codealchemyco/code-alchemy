defmodule CodeAlchemy.UserFactory do
  defmacro __using__(_opts) do
    quote do
      def user_factory do
        %CodeAlchemy.User{
          first_name: sequence(:name, &"first_name#{&1}"),
          last_name: sequence(:name, &"last_name#{&1}"),
          email: sequence(:email, &"test_#{&1}@codealchemy.co"),
          password: "test1234",
          password_confirmation: "test1234"
        }
      end
    end
  end
end
