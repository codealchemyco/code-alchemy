defmodule CodeAlchemy.BranchFactory do
  defmacro __using__(_opts) do
    quote do
      def branch_factory do
        %CodeAlchemy.Branch{
          name: sequence(:name, &"name#{&1}"),
          repository: build(:repository)
        }
      end
    end
  end
end
