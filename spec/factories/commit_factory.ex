defmodule CodeAlchemy.CommitFactory do
  defmacro __using__(_opts) do
    quote do
      def commit_factory do
        %CodeAlchemy.Commit{
          hash: Ecto.UUID.generate |> binary_part(0, 20),
          author_name: sequence(:author_name, &"author_name#{&1}"),
          author_email: sequence(:author_email, &"test_#{&1}@codealchemy.co"),
          created_at: DateTime.utc_now,
          message: sequence(:message, &"message#{&1}"),
          branch: build(:branch)
        }
      end
    end
  end
end
