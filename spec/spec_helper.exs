{:ok, _} = Application.ensure_all_started(:ex_machina)

Ecto.Adapters.SQL.Sandbox.mode(CodeAlchemy.Repo, :manual)

ESpec.configure fn(config) ->
  config.before fn(tags) ->
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(CodeAlchemy.Repo, ownership_timeout: 30_000_000)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(CodeAlchemy.Repo, {:shared, self()})
    end
  end

  config.finally fn(_shared) ->
    Ecto.Adapters.SQL.Sandbox.checkin(CodeAlchemy.Repo, [])
  end
end
