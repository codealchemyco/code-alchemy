# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :code_alchemy,
  ecto_repos: [CodeAlchemy.Repo]

# Configures the endpoint
config :code_alchemy, CodeAlchemyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: CodeAlchemyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CodeAlchemy.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configures db
config :code_alchemy, CodeAlchemy.Repo,
  adapter: Ecto.Adapters.Postgres,
  migration_timestamps: [type: :utc_datetime],
  pool_size: 10

# Configures Guardian
config :code_alchemy, CodeAlchemy.Auth.Guardian,
  issuer: "code_alchemy",
  secret_key: System.get_env("GUARDIAN_SECRET_KEY")

config :ueberauth, Ueberauth,
  providers: [
    github: {Ueberauth.Strategy.Github, [default_scope: "user,public_repo"]}
  ]

# Configures Github OAuth
config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID"),
  client_secret: System.get_env("GITHUB_CLIENT_SECRET")

# Configures file uploading
config :arc,
  storage: Arc.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
