use Mix.Config

# Configure your database
config :code_alchemy, CodeAlchemy.Repo,
  url: System.get_env("DATABASE_URL"),
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :code_alchemy, CodeAlchemyWeb.Endpoint,
  http: [port: 4002],
  server: false

# Configures mailer
config :code_alchemy, CodeAlchemy.Mailer,
  adapter: Bamboo.TestAdapter

# Print only warnings and errors during test
config :logger, level: :warn
