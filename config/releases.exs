# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
import Config

config :code_alchemy, CodeAlchemy.Repo,
  url: System.get_env("DATABASE_URL")

config :code_alchemy, CodeAlchemyWeb.Endpoint,
  http: [:inet6, port: 4000],
  secret_key_base: System.get_env("SECRET_KEY_BASE")

# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.

config :peerage, via: Peerage.Via.Dns,
  dns_name: System.get_env("SERVICE_NAME"),
  app_name: "code_alchemy"

config :code_alchemy, CodeAlchemy.Auth.Guardian,
  secret_key: System.get_env("GUARDIAN_SECRET_KEY")

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: System.get_env("GITHUB_CLIENT_ID"),
  client_secret: System.get_env("GITHUB_CLIENT_SECRET")
